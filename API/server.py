#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.ioloop
import tornado.web
import tornado.escape
import requests
import json
from abc import abstractmethod


MATERIALS_URL = "https://uchebnik.mos.ru/cms/api/materials?per_page=10&sort_column=relevance&sort_direction=desc&with_controllable_items=true&search={}&types=lesson_templates,test_specifications"

def load_users_info():
    path_to_storage = "users/users.json"
    with open(path_to_storage, "r", encoding="utf8") as f:
        return json.load(f)


class AuthHelper():
    def __init__(self):
        url = "https://uchebnik.mos.ru/api/sessions"

        payload = {"login": "vysbor2009", "password_hash": "f704d46b66894bb28b3a6b7286235db7","password_hash2": "ea5f9cd4016a7b5c72a7126b57d902ce"}
        headers = {
            'Content-Type': "application/json",
            'Connection': "keep-alive",
            'Accept': "application/json; charset=UTF-8",
            'Content-Length': "130",
        }

        response = requests.post(url, json=payload, headers=headers).json()
        self.auth_token = response["authentication_token"]
        self.user_id = response["profiles"][0]["id"]

    def get_headers(self):
        headers = {
            'User-Id': self.user_id,
            'Profile-Id': self.user_id,
            'Accept': "application/vnd.cms-v4+json",
            'Cookie': "mos_id=CllGxlqXLmE1806llGCcAgA=; udacl=resh; request_method=GET; ",
            'Connection': "keep-alive",
            'Content-Type': "application/json",
            'Auth-Token': self.auth_token
        }
        return headers


USERS_DATA = load_users_info()
AUTH_HElPER = AuthHelper()


def find_user(user_id):
    for user in USERS_DATA:
        if user["user_id"] == user_id:
            return user


def get_avg_score(user_id):
    user_info = find_user(user_id)
    if user_info is None:
        return None
    result = {}
    for subject in user_info["scores"]:
        subject_id = subject["subject_id"]
        scores = [lesson["score"] for lesson in subject["lessons"] if lesson["score"] > 0]
        if len(scores) == 0:
            continue
        result[subject_id] = sum(scores) / len(scores)

    return result


def get_lesson(lesson_id):
    api_url = "https://uchebnik.mos.ru/cms/api/lesson_templates/{}"
    response = requests.request("GET", api_url.format(lesson_id), headers=AUTH_HElPER.get_headers())
    if response.status_code == 200:
        return response.json()


def get_top_lessons(subject_ids="46,39,4", types="game_apps,lesson_templates"):
    api_url = "https://uchebnik.mos.ru/cms/api/materials"
    params = {"subject_ids": subject_ids, "types": types, "page": 1, "per_page": 5,
              "with_rating": "true", "sort_column": "average_rating", "sort_direction": "desc"}
    response = requests.request("GET", api_url, headers=AUTH_HElPER.get_headers(), params=params)
    if response.status_code == 200:
        return response.json()


def make_search_request(s):
    response = requests.request("GET", MATERIALS_URL.format(s), headers=AUTH_HElPER.get_headers())
    if response.status_code == 200:
        return response.json()


class EdurectHandler(tornado.web.RequestHandler):
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.payload = None

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def write_error(self, status_code, **kwargs):
        self.finish({"code": status_code, "message": self._reason})

    def prepare(self):
        try:
            self.payload = tornado.escape.json_decode(self.request.body)
        except:
            pass

    def options(self):
        # no body
        self.set_status(204)
        self.finish()


class LessonsFilterHandler(EdurectHandler):
    @abstractmethod
    def filter_lesson(self, lesson):
        pass

    def get(self):
        try:
            if self.payload is not None:
                user_id = self.payload["user_id"]
            else:
                user_id = int(self.get_argument("user_id"))
        except:
            self.send_error(400)
            return
        if self.payload is not None:
            subject_id = self.payload.get("subject_id", None)
        else:
            subject_id = self.get_argument("subject_id", None)

        if subject_id is not None:
            subject_id = int(subject_id)

        user_info = find_user(user_id)

        if user_info is None:
            self.send_error(404)
            return

        new_user_info = {"user_id": user_id, "scores": []}

        for subject in user_info["scores"]:
            if subject_id is not None and subject_id != subject["subject_id"]:
                continue
            lessons = subject["lessons"]
            new_lessons = []

            for lesson in lessons:
                if self.filter_lesson(lesson):
                    lesson_id = lesson["id"]
                    data = get_lesson(lesson_id)
                    if data:
                        lesson["data"] = data
                    new_lessons.append(lesson)

            if len(new_lessons) > 0:
                new_user_info["scores"].append({"subject_id": subject["subject_id"], "lessons": new_lessons})

        self.write(new_user_info)




class MissedLessonsHandler(LessonsFilterHandler):
    def filter_lesson(self, lesson):
        return lesson["score"] < 0


class BadScoresLessonsHandler(LessonsFilterHandler):
    def filter_lesson(self, lesson):
        return 0 < lesson["score"] < 4


class LoginHandler(EdurectHandler):
    def get(self):
        try:
            if self.payload is not None:
                user_id = self.payload["user_id"]
            else:
                user_id = int(self.get_argument("user_id"))
        except:
            self.send_error(400)
            return
        user_info = find_user(user_id)

        if user_info is None:
            self.send_error(404)
            return
        self.write({"user_id": user_id, "name": user_info["name"], "avg_scores": get_avg_score(user_id),
                    "type": user_info["type"]})


class PopularMaterialsHandler(EdurectHandler):
    def get(self):
        try:
            if self.payload is not None:
                user_id = self.payload["user_id"]
            else:
                user_id = int(self.get_argument("user_id"))
        except:
            self.send_error(400)
            return
        user_info = find_user(user_id)

        if user_info is None:
            self.send_error(404)
            return

        self.write({"user_id": user_id, "data": get_top_lessons()})


class SimilarityFinder(EdurectHandler):
    def get(self):
        output = make_search_request(u"логарифм")
        self.write({"data": output})


class PreparingMaterialsHandler(EdurectHandler):
    def get(self):
        try:
            if self.payload is not None:
                user_id = self.payload["user_id"]
            else:
                user_id = int(self.get_argument("user_id"))
        except:
            self.send_error(400)
            return
        user_info = find_user(user_id)

        if user_info is None:
            self.send_error(404)
            return

        self.write({"user_id": user_id, "data": get_top_lessons(subject_ids="45", types="lesson_templates")})


if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/missed-lessons/?", MissedLessonsHandler),
        (r"/bad-score-lessons/?", BadScoresLessonsHandler),
        (r"/login/?", LoginHandler),
        (r"/top-lessons/?", PopularMaterialsHandler),
        (r"/similar-materials/?", SimilarityFinder),
        (r"/preparing-materials/?", PreparingMaterialsHandler)
        ])

    application.listen(8888)
    tornado.ioloop.IOLoop.current().start()

